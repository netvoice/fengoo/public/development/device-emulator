# inspired by https://github.com/eclipse/paho.mqtt.python/blob/master/examples/client_pub-wait.py

import json
import sys
from modules.mqtt_publisher import publish_message as publish_mqtt
from modules.web_socket_publisher import publish_message as publish_web_socket
from modules.rest_v1_publisher import publish_message as publish_rest_v1
from modules.rest_v2_publisher import publish_message as publish_rest_v2
from modules.rest_v3_publisher import publish_message as publish_rest_v3

if len(sys.argv) < 2 or len(sys.argv) == 3 or len(sys.argv) > 5:
    print("Usage:")
    print(" \"python3 device-emulator-publisher.py <config-file.json>\" - Lists all available reports/events.")
    print(" \"python3 device-emulator-publisher.py <config-file.json> report <report name> [message]\" - Sends the report.")
    print(" \"python3 device-emulator-publisher.py <config-file.json> event <event name> [message]\" - Sends the event.")
    exit(0)


f = open(sys.argv[1],)
device_config = json.load(f)
f.close()
reports = device_config['reports']
events = device_config['events']

print(f"Device info can be found on https://app.fengoo.cz/#/devices/detail/{device_config['_device_id']}\n")

if len(sys.argv) == 2:
    print(f"Available reports: {', '.join(list(reports.keys()))}\n")

    for key, value in reports.items():
        print(f"{key} message example:\n  {json.dumps(value['message_example'])}\n")

    print("Usage: \"python3 mqtt-device-publisher.py <config-file.json> report <report name> [message]\" - Sends the report. If no message is specified, the example from above is used.")
    print()
    print()
    print(f"Available events: {', '.join(list(events.keys()))}\n")

    for key, value in events.items():
        print(f"{key} message example:\n  {json.dumps(value['message_example'])}\n")

    print("Usage: \"python3 mqtt-device-publisher.py <config-file.json> event <event name> [message]\" - Sends the event. If no message is specified, the example from above is used.")
    exit(0)


action_type = sys.argv[2]
action_name = sys.argv[3]
if action_type == 'report':
    action = reports[action_name]
else:
    if action_type == 'event':
        action = events[action_name]
    else:
        raise Exception(f"Unsupported message type: {action_type}")


if len(sys.argv) == 4:
    print("Using predefined example message.\n")
    msg = action['message_example']
else:
    msg = json.loads(sys.argv[4])
    print(f"Message: {msg}") 


if device_config['connector'] == 'mqtt':
    publish_mqtt(device_config['auth_name'], device_config['auth_password'], action_type, action_name, json.dumps(msg))
else:
    if device_config['connector'] == 'web-socket':
        publish_web_socket(device_config['auth_name'], device_config['auth_password'], action_type, action_name, json.dumps(msg))
    else:
        if device_config['connector'] == 'rest-v1':
            publish_rest_v1(device_config['auth_name'], device_config['auth_password'], action_type, action_name, json.dumps(msg))
        else:
            if device_config['connector'] == 'rest-v2':
                publish_rest_v2(device_config['auth_name'], device_config['auth_password'], action_type, action_name, json.dumps(msg))
            else:
                if device_config['connector'] == 'rest-v3':
                    publish_rest_v3(device_config['auth_name'], device_config['auth_password'], action_type, action_name, json.dumps(msg))
                else:
                    raise Exception(f"Unsupported connector type: {device_config['connector']}")
