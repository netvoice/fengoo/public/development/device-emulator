# inspired by https://github.com/eclipse/paho.mqtt.python/blob/master/examples/client_pub-wait.py

import uuid
import json
import time
import paho.mqtt.client as mqtt
import sys
from modules.mqtt_subscriber import subscribe as subscribe_mqtt
from modules.web_socket_subscriber import subscribe as subscribe_web_socket

def exit_app():
    print("Exiting application")
    exit()


if len(sys.argv) != 2:
    print("Usage:")
    print(" \"python3 device-emulator-consumer.py <config-file.json>\"")
    exit(0)

f = open(sys.argv[1],)
device_config = json.load(f)
f.close()

print(f"Listening to commands sent to device {device_config['_device_id']} - https://app.fengoo.cz/#/devices/detail/{device_config['_device_id']}")


try:
    if device_config['connector'] == 'mqtt':
        subscribe_mqtt(device_config['auth_name'], device_config['auth_password'])
    else:
        if device_config['connector'] == 'web-socket':
            subscribe_web_socket(device_config['auth_name'], device_config['auth_password'])
except KeyboardInterrupt:
    exit_app()

