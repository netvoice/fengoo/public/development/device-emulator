import requests
import json

def publish_message(auth_name, auth_password, action_type, action_name, msg):
    address = 'https://device.fengoo.cz/rest/reports'

    print(f"Sending message '{msg}'")
    x = requests.post(address, data = msg, auth = (auth_name, auth_password))
    print(f"Message sent: {x.status_code}")


if __name__ == "__main__":
    f = open('../thermometer.json')
    device_config = json.load(f)
    f.close()

    report = device_config['reports']['default']
    publish_message(device_config['auth_name'], device_config['auth_password'], 'report', 'default', json.dumps(report['message_example']))
