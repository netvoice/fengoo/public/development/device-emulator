import websocket
import base64
import json

def publish_message(auth_name, auth_password, action_type, action_name, msg):
    address = 'wss://device.fengoo.cz/ws/v2'
    credentials = f'{auth_name}:{auth_password}'


    ws = websocket.WebSocket()
    ws.connect(address, header = {'Authorization: Basic ' + base64.b64encode(bytes(credentials, encoding='utf-8')).decode('utf-8')})

    print(f"Sending message '{msg}'")
    ws.send(msg)
    print("Message sent")

    ws.close()

if __name__ == "__main__":
    f = open('../watermeter.json')
    device_config = json.load(f)
    f.close()

    report = device_config['reports']['default']
    publish_message(device_config['auth_name'], device_config['auth_password'], 'report', 'default', json.dumps(report['message_example']))
