import uuid
import paho.mqtt.client as mqtt

def subscribe(auth_name, auth_password):
    broker = 'mqtt.fengoo.cz'
    port = 1883
    client_id = f'fengoo-device-{uuid.uuid4()}'
    topic = f'fengoo/device/v1/{auth_name}/p1/command/#'
    status_topic = f'fengoo/device/v1/{auth_name}/p1/online'


    def on_connect(mqttc, obj, flags, rc):
        print(f"Connection result: {rc}")
        mqttc.publish(status_topic, payload='1', qos=0, retain=True)

    def on_message(mqttc, obj, msg):
        print(f"Received message '{msg.payload.decode()}' in topic {msg.topic}")

    def on_subscribe(mqttc, obj, mid, granted_qos):
        print("Subscribed")

    def on_log(mqttc, obj, level, string):
        print(string)

    def on_disconnect(mqttc, obj, rc):
        print("Disconnected")

    
    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, client_id)
    client.username_pw_set(auth_name, auth_password)
    client.on_message = on_message
    client.on_connect = on_connect
    client.on_subscribe = on_subscribe
    # Uncomment to enable debug messages
    # client.on_log = on_log
    client.on_disconnect = on_disconnect
    client.will_set(status_topic, payload='0', qos=0, retain=True)
    client.connect(broker, port)
    print(f"Subscribing to topic {topic}")
    client.subscribe(topic, 1)
    client.loop_forever()



