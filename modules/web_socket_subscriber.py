# Sample application emulating websocket device.
# Inspired by https://github.com/websocket-client/websocket-client#long-lived-connection
#
# Requirements:
# python3, pip3 (python package installer)
#
# Install websocket-client library (https://pypi.org/project/websocket-client):
# pip3 install websocket-client
#
# After web socket is open, application waits and prints incoming messages (manually sent from web interface).


import websocket
import base64
import _thread as thread
import time


def subscribe(auth_name, auth_password):
    credentials = auth_name + ':' + auth_password
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(
        'wss://device.fengoo.cz/ws/v2',
        on_message = on_message,
        on_error = on_error,
        on_close = on_close,
        on_open = on_open,
        header = {'Authorization: Basic ' + base64.b64encode(bytes(credentials, encoding='utf-8')).decode('utf-8')})

    # Reopen websocket on close (https://github.com/websocket-client/websocket-client/issues/95)
    websocket.setdefaulttimeout=1
    while True:
        try:
            # Client has to send ping (heartbeat) periodically. Otherwise the connection is timed out by azure after 60 seconds.
            ws.run_forever(ping_interval=50, ping_timeout=10)
        except Exception:
            pass


def on_open(ws):
    print("Connection opened, sending sample message")

def on_message(ws, message):
    print(f"New message received: {message}")

def on_close(ws):
    print("Connection closed")

def on_error(ws, error):
    if isinstance(error, KeyboardInterrupt):
        # vyhodim stejnou vyjimku vys
        raise error
    else:
        print("Error occured")
        print(error)
        # Wait for 1 second. Do not try to continue immediately, it would probably cause 100% CPU load.
        time.sleep(1)

if __name__ == "__main__":
    subscribe('UUUUUUUUU402', '4lhk14NpRDUUUUUUUUUUUUUUUUUUUUUU')
