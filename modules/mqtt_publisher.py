import json
import uuid
import paho.mqtt.client as mqtt


def publish_message(auth_name, auth_password, action_type, action_name, msg):
    broker = 'mqtt.fengoo.cz'
    port = 1883
    client_id = f'fengoo-device-{uuid.uuid4()}'
    topic = f'fengoo/device/v1/{auth_name}/p1/{action_type}/{action_name}'
    
    def on_connect(mqttc, obj, flags, rc):
        print(f"Connection result: {rc}")

    def on_message(mqttc, obj, msg):
        print(f"Received message '{msg.payload.decode()}' in topic {msg.topic}")

    def on_publish(mqttc, obj, mid):
        print("Message published")

    def on_log(mqttc, obj, level, string):
        print(string)

    def on_disconnect(mqttc, obj, rc):
        print("Disconnected")

    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1, client_id)
    client.username_pw_set(auth_name, auth_password)
    client.on_message = on_message
    client.on_connect = on_connect
    client.on_publish = on_publish
    # Uncomment to enable debug messages
    # client.on_log = on_log
    client.on_disconnect = on_disconnect
    client.connect(broker, port)
    client.loop_start()


    message_info = client.publish(topic, msg, 1, False)
    if message_info.rc == 0:
        print(f"Sending message '{msg}' to the topic {topic}")
    else:
        print(f"Failed to send message to the topic {topic}")

    message_info.wait_for_publish()


if __name__ == "__main__":
    f = open('../smart-plug-xpablo.json')
    device_config = json.load(f)
    f.close()

    report = device_config['reports']['measurement']
    publish_message(device_config['auth_name'], device_config['auth_password'], 'report', 'measurement', json.dumps(report['message_example']))
