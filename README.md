# Simple device emulator

Sada jednoduchých python skriptů pro emulaci zařízení. Tím se výrazně liší od komplexního projektu https://gitlab.com/netvoice/fengoo/incubator/device-emulator

Utility `device-emulator-consumer` a `device-emulator-producer` umožňují emulovat zařízení založená na MQTT. Informace o zařízení jsou načítány z externích json souborů.

## Instalace

```sh
pip3 install paho-mqtt websocket
```

## device-emulator-publisher
Slouží k otestování odesílání zpráv ze zařízení do fengoo. Příkaz

```
python3 device-emulator-publisher.py smart-plug-xpablo.json
```

vypíše dostupné akce zařízení smart-plug-xpablo a ukázkové zprávy. Např.:

```
Device info can be found on https://app.fengoo.cz/#/devices/detail/1110

Available actions: telemetry_report, default_report, start_up_event, turned_on_event, turned_off_event

telemetry_report message example:
  {"values": {"uptime": 81, "freeRam": 1078452, "maxContiguousBlock": 648452, "heapFragmentation": 12, "rssi": -51}}

default_report message example:
  {"values": {"voltage": 230.1, "current": 0.15, "power": 30.1, "energy": 168.4, "apparentPower": 45, "powerFactor": 0.7, "state": 1}}

start_up_event message example:
  {"values": {"resetReason": 0, "hardwareType": "spx-1", "fwVersion": "1.0.3", "applicationName": "app", "hardware": "x-1", "networkName": "wifi-1548775"}}

turned_on_event message example:
  {"values": {"source": 0}}

turned_off_event message example:
  {"values": {"source": 1}}

Usage: "python3 mqtt-device-publisher.py <config-file.json> <action> [message]" - Sends the action. If no message is specified, the example above is used.
```

Ve výpisu je vidět URL, na kterém se nachází odpovídající zařízení ve fengoo (dostupné i pod účtem demo/demo). Dál je tam seznam akcí a ukázkové zprávy.


Následujícím příkazem se pošle událost turned_on, kde se použije předdefinovaná ukázková zpráva z výpisu uvedeného výše.
```
python3 device-emulator-publisher.py smart-plug-xpablo.json turned_on_event
```

Namísto předdefinované zprávy je možné připojit vlastní json, např.
```
python3 device-emulator-publisher.py smart-plug-xpablo.json turned_on_event '{"values": {"source": 1}}'
```

## device-emulator-consumer
Spouští příkazem

```
python3 device-emulator-consumer.py smart-plug-xpablo.json
```
a poté čeká na povely, které přijdou z fengoo, a ty následně vypisuje na obrazovku.
